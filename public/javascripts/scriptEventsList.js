function del_event(event_id) {
    if (confirm('Вы уверены, что хотите удалить мероприятие?')) {
        $.post("/listener/del_event",
            {
                event_id: event_id
            },
            function (data, status) {
                if (status == 'success') {
                    var response = data;
                    location.reload();
                }
            });
    }
}