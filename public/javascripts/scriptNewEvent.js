function show(){
    var person_income_form = document.getElementById('income_per_person');
    var cost_adding = document.getElementById('costs_adding');
    var tip =  document.querySelector('[name="optionsRadios"]:checked').value;
    if(tip == 'option1'){
        person_income_form.style.display = 'none';
    }else {
        person_income_form.style.display = 'block';
    }
}

function choose_part_input() {
    var single_input_form = document.getElementById('single_input');
    var paste_input_form = document.getElementById('paste_input');

    var tip =  document.querySelector('[name="part_input_opt"]:checked').value;
    if(tip == 'option1'){
        single_input_form.style.display = 'block';
        paste_input_form.style.display = 'none';

    }else {
        single_input_form.style.display = 'none';
        paste_input_form.style.display = 'block';
    }
}

document.getElementById("save").addEventListener("click", save_event_data);

function save_event_data() {
    var event_name = document.getElementById('event_name').value;
    if (event_name.length == 0){document.getElementById('event_name').style.border = '1px solid red';return}
    var option =  document.querySelector('[name="optionsRadios"]:checked').value;
    var participants = get_partNames_from_list();
    if (participants.length == 0){document.getElementById('list_info').style.color = 'red';return}
    var income = 0;
    var budget;
    var part_debt;
    var part_data = [];
    var cost_data = get_costs_from_table();
    if(option == 'option1'){
        budget = cost_data.budget;
        part_debt = budget / participants.length;
    }else if (option == 'option2'){
        budget = parseFloat(document.getElementById('budget').value);
        if (isNaN(budget)){document.getElementById('budget').style.border = '1px solid red';return}
        part_debt = budget;
        budget *= participants.length;
    }

    for (var i = 0; i < participants.length; i++){
        part_data.push({'name': participants[i], 'debt': part_debt.toFixed(2), 'income': 0})
    }


    var dataForSave = JSON.stringify({'event_name': event_name, 'budget': budget, 'debt': budget,
        'costs': cost_data.costs_list, 'participants': part_data, 'income': income, 'modified': Date.now()});

    $.post("/listener/create_event",
        {
            dataForSave: dataForSave
        },
        function(data,status){
            if (status == 'success') {
                var response = data;
                localStorage.setItem(response, dataForSave);
                window.location.href = "/event_menu/" +response;
            }
        });
}

$(document).ready(function(){
    $('#participant_name').keydown(function(){
        if(event.keyCode==13)
       {
           add_part_to_list(this.value);
           this.value = '';
       }
    });
});

function add_part_to_list(name) {
    var part_list = document.getElementById('part_list');
    var li = document.createElement("li");
    var span_for_del = document.createElement("span");
    span_for_del.className = "glyphicon glyphicon-remove";
    span_for_del.style.color = '#b92c28';
    span_for_del.addEventListener('click', function(){this.parentNode.remove();});
    li.appendChild(document.createTextNode(name));
    li.appendChild(span_for_del);
    part_list.appendChild(li);
}

function get_partNames_from_list() {
    var part_list = document.getElementById('part_list');
    var participants = part_list.getElementsByTagName('li');
    var list = [];
    var tip =  document.querySelector('[name="part_input_opt"]:checked').value;
    if(tip == 'option1'){
        for (var i = 0; i < participants.length; i++){
        var row_text = (participants[i].innerHTML).trim();
        list.push(row_text.substr(0, row_text.indexOf('<')));
        }
    }else {
        var big_part_list = document.getElementById('big_part_list').value.replace('\r','').split('\n');
        for (var f = 0; f < big_part_list.length; f++){
        list.push(big_part_list[f]);
        }
    }

    return list
}

function add_cost_row() {
    var costs_table = document.getElementById('costs_table');
    var row = document.createElement("TR");

    var td1 = document.createElement("TD");
    var td2 = document.createElement("TD");
    var input_name = document.createElement('input');
    input_name.className = 'form-control';
    input_name.setAttribute('type', 'text');
    input_name.setAttribute('name', 'cost_name');
    var input_sum = document.createElement('input');
    input_sum.className = 'form-control';
    input_sum.setAttribute('type', 'number');
    input_sum.setAttribute('name', 'cost_value');
    td1.appendChild(input_name);
    td2.appendChild(input_sum);

    row.appendChild(td1);
    row.appendChild(td2);
    row.addEventListener('dblclick', function (){this.remove()});

    costs_table.appendChild(row);
}

function get_costs_from_table() {
    var cost_names = document.getElementsByName('cost_name');
    var cost_values = document.getElementsByName('cost_value');
    var costs_list = [];
    var budget = 0;
    for (var i = 0; i < cost_names.length; i++){
        var cost_val = cost_values[i].value;
        if (!cost_val){
            cost_val = '0';
        }
        costs_list.push({cost_name: cost_names[i].value, cost_value: cost_val});
        budget += parseFloat(cost_val);
    }
    return {costs_list: costs_list, budget:budget}

}