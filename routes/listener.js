var express = require('express');
var router = express.Router();
var mongo = require("../mongo");
var multer = require('multer');
var upload = multer();


router.post('/create_event', upload.array(), function(req, res, next) {
    if (req.session.username) {
    	var username = req.session.username;
    	mongo.createEvent(username, JSON.parse(req.body.dataForSave), function(data){
    	res.send(data._id);
    	});
    }else{
    	res.send('error');
    }
});

router.post('/get_event_data', upload.array(), function(req, res, next) {
    if (req.session.username) {
        var username = req.session.username;
        mongo.getEventData(username, req.body.event_id, function(data){
            res.json(data);
        })
    }else{
        res.send('error');
    }
});

router.post('/update_event_data', upload.array(), function(req, res, next) {
    if (req.session.username) {
        var username = req.session.username;
        var event_data = JSON.parse(req.body.event_data);
        mongo.updateEventData(username, event_data.event_id, event_data,
        function(data){
            res.json(data);
        })
    }else{
        res.send('error');
    }
});

router.post('/del_event', upload.array(), function(req, res, next) {
    if (req.session.username) {
        var username = req.session.username;
        var event_id = req.body.event_id;
        mongo.delEvent(username, event_id,
            function(data){
                res.json(data);
            })
    }else{
        res.send('error');
    }
});

module.exports = router;
