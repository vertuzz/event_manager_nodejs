var express = require('express');
var router = express.Router();
var mongo = require("../mongo");
var multer = require('multer');
var upload = multer();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Event Manager', username: req.session.username });
});

router.get('/events_list', function(req, res, next) {
	if (req.session.username) {
        var username = req.session.username;
        mongo.getEventsList(username, function(data){
        res.render('events_list', { title: 'Список мероприятий', 
        							username: req.session.username,
        							data: data });
        });
    }else{
		res.redirect('/');
    }
  
});

router.get('/about', function(req, res, next) {
  res.render('about', { title: 'О проекте', username: req.session.username });
});

router.get('/new_event', function(req, res, next) {
	if (req.session.username) {
		res.render('new_event', { title: 'Новое мероприятие', username: req.session.username });
	}else{
		res.redirect('/');
	}
});

router.get('/event_menu/:id', function(req, res, next) {
	if (req.session.username) {
		res.render('event_menu', { title: 'Управление мероприятием', username: req.session.username });
	}else{
		res.redirect('/');
	}
});

router.get('/register', function(req, res, next) {
  res.render('register', { title: 'Регистрация', username: req.session.username });
});

router.post('/register', upload.array(), function (req, res, next) {
  mongo.findUserByName(req.body.username ,function (data) {
  		var mess = [];
        if (data != null) {
        	mess.push("Имя уже занято");
        	res.render('register', {title: 'Регистрация',mess:mess});
        }else{
        	mongo.createNewUser(req.body.username, req.body.pass, function(data){
        		req.session.username = data.name;
        		res.redirect('/');
        	});
        }
    });
});

router.get('/login', function(req, res, next) {
  res.render('login', { title: 'Вход', username: req.session.username });
});

router.post('/login', upload.array(), function (req, res, next) {
	mongo.verifyUser(req.body.username, req.body.pass, function(data){
		if (!data) {
			res.render('login', {title: 'Вход', mess: ["Неправильная комбинация логина и пароля"],
		username: req.session.username});
		}else{
			req.session.username = req.body.username;
			res.redirect('/');
		}
	})
});

router.get('/logout', function(req, res, next) {
	req.session.username = undefined;
	res.redirect('/');
});

module.exports = router;
