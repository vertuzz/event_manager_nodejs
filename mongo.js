var mongojs = require('mongojs');
var db = mongojs('mongodb://event:event@ds019123.mlab.com:19123/event');
var passwordHash = require('password-hash');

function findUserByName(name, callback) {
    var users = db.collection('users');
    users.findOne({"name": name},function (err, data) {
        callback(data);
    });
}

function createNewUser(name, pass, callback) {
	var hashedPassword = passwordHash.generate(pass);
	var users = db.collection('users');
	users.insert({'name': name, 'pass': hashedPassword}, function(err, data){
		callback(data);
	})
}

function verifyUser(name, pass, callback){
	var users = db.collection('users');
	users.findOne({"name": name},function (err, data) {
    	var unHashedPassword = passwordHash.verify(pass, data.pass);
    	callback(unHashedPassword);
    });

}

function createEvent(username, event_data, callback) {
	var user_events = db.collection(username + '_events');
	user_events.insert(event_data, function(err, data){
		if (err) throw err;
		callback(data);
	})
}

function getEventData(username, id, callback) {
	var user_events = db.collection(username + '_events');
	user_events.findOne({_id: mongojs.ObjectId(id)}, function(err, data){
		if (err) throw err;
		callback(data);
	})
}

function getEventsList(username, callback) {
	var user_events = db.collection(username + '_events');
	user_events.find(function(err, data){
		if (err) throw err;
		callback(data);
	})
}

function delEvent(username, event_id, callback) {
	var user_events = db.collection(username + '_events');
	user_events.remove({_id: mongojs.ObjectId(event_id)}, function(err, data){
		if (err) throw err;
		callback(data);
	});
}

function updateEventData(username, id, new_event_data, callback) {
	var user_events = db.collection(username + '_events');
	user_events.findAndModify({
	query: {_id: mongojs.ObjectId(id)},
	update: { $set: {debt: new_event_data['debt'],
                   income: new_event_data['income'],
                   budget: new_event_data['budget'],
                   participants: new_event_data['participants'],
                   costs: new_event_data['costs'],
                   modified: new_event_data['modified']} },
	new: true},
	function(err, data, lastErrorObject){
		if (err) throw err;
		callback(data);
	})
}

module.exports.findUserByName = findUserByName;
module.exports.createNewUser = createNewUser;
module.exports.verifyUser = verifyUser;
module.exports.createEvent = createEvent;
module.exports.getEventData = getEventData;
module.exports.updateEventData = updateEventData;
module.exports.getEventsList = getEventsList;
module.exports.delEvent = delEvent;